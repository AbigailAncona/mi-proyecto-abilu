package principal;

import controller.NuevaPersonaController;
import model.daos.IPersonaDAO;
import model.daos.PersonaDAO;
import model.daos.PersonaOracleDAO;
import view.PersonaView;
import view.PersonaViewGreen;

public class ProyectoPersona {

	public static void main(String[] args) {
	
		
		IPersonaDAO dao = new PersonaDAO();
		PersonaView vista = new PersonaViewGreen();
		vista.setVisible(true);
		NuevaPersonaController controller = new NuevaPersonaController(dao, vista);

	}
}
