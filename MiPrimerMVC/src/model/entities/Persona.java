package model.entities;

//objetos de modelo o entidades

public class Persona {
	
	
//atributos de la clase persona
	private int id;
	private String curp;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	
//	Constructor de la clase persona
	public Persona() {
		
	}


	public Persona(String curp, String nombre, String apellidoPaterno, String apellidoMaterno) {
	super();
	this.curp = curp;
	this.nombre = nombre;
	this.apellidoPaterno = apellidoPaterno;
	this.apellidoMaterno = apellidoMaterno;
}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getCurp() {
		return curp;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	
}