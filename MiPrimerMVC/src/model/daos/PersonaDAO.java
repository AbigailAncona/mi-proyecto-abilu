package model.daos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.database.DatabaseConnection;
import model.database.MySQLConnection;
import model.entities.Persona;

public class PersonaDAO implements IPersonaDAO {
	// MySQLConnection connection;
	//
	// public PersonaDAO(){
	//
	// connection = new MySQLConnection();
	// connection.connect();

	@Override
	public void agregarPersona(Persona p) throws SQLException {
		DatabaseConnection con = DatabaseConnection.getInstance();
		PreparedStatement ps;
		ps = con.getConnection().prepareStatement(
				"INSERT INTO personas(id, curp, nombre, apellidoPaterno,apellidoMaterno)VALUES (null,?,?,?,?)");
		ps.setString(1, p.getCurp());
		ps.setString(2, p.getNombre());
		ps.setString(3, p.getApellidoPaterno());
		ps.setString(4, p.getApellidoMaterno());
		ps.executeUpdate();
		ps.close();
	}

	@Override
	public void modificarPersona(Persona p) {
		// try {
		// System.out.println("Se ha modificado correctamente");
		//
		// }catch(Exception e){
		// System.out.println("Problemas al realizar la modificacion");
		//
		// }
		System.out.println("Persona modificada correctamente");

	}

	@Override
	public void eliminarPersona(Persona p) {
		// try {
		// System.out.println("Se ha elimando correctamente");
		//
		// }catch(Exception e){
		// System.out.println("Problemas al eliminar");
		//
		// }

	}

	@Override
	public Persona buscarPorCurp(String curp) {
		// try {
		// System.out.println("consulta ejecutada exitosamente");
		// return null;
		// }catch(Exception e){
		// System.out.println("Problemas al realizar la busqueda");
		return null;

	}

	@Override
	public List<Persona> buscarPorNombre(String nombre) {
		 try {
		 System.out.println("Se ha realizado correctamente la busqueda");
		
		 }catch(Exception e){
		 System.out.println("Problemas al realizar la busqueda");
		
		 }
		return null;
	}

	public List<Persona> buscarTodos() {
		List<Persona> list = new ArrayList<Persona>();
		try {
			DatabaseConnection con = DatabaseConnection.getInstance();
			Statement statement = con.getConnection().createStatement();
			ResultSet result = statement.executeQuery("SELECT * " + "FROM personas");
			while (result.next()) {
				Persona p = new Persona();
				p.setId(result.getInt("id"));
				p.setCurp(result.getString("curp"));
				p.setNombre(result.getString("nombre"));
				p.setApellidoPaterno(result.getString("apellidoPaterno"));
				p.setApellidoMaterno(result.getString("apellidoMaterno"));
				list.add(p);
			}
			statement.close();
			result.close();
			return list;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
