package model.daos;

import java.sql.SQLException;
import java.util.List;

import model.entities.Persona;

public interface IPersonaDAO {

	 public void agregarPersona(Persona p) throws SQLException;
	 
	 public void modificarPersona(Persona p);
	 
	 public void eliminarPersona(Persona p);
	 
	 public Persona buscarPorCurp(String curp);
	 
	 public List<Persona> buscarPorNombre(String nombre);


	 
}
