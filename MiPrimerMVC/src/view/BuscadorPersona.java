package view;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
public class BuscadorPersona extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JButton guardarBtn;
	private JTextField buscarRAMTb;
	private JLabel lblBuscarRam;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscadorPersona frame = new BuscadorPersona();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public BuscadorPersona() {
		setTitle("Buscador de Personas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 569, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(92, 53, 326, 183);
		contentPane.add(scrollPane);
		
		guardarBtn = new JButton("Nuevo");
		guardarBtn.setBounds(425, 91, 107, 23);
		contentPane.add(guardarBtn);
		
		JButton btnNewButton = new JButton("modificar");
		btnNewButton.setBounds(425, 125, 107, 23);
		contentPane.add(btnNewButton);
		
		JButton btnEliminar = new JButton("eliminar");
		btnEliminar.setBounds(425, 160, 110, 23);
		contentPane.add(btnEliminar);
		
		buscarRAMTb = new JTextField();
		buscarRAMTb.setColumns(10);
		buscarRAMTb.setBounds(92, 28, 326, 20);
		contentPane.add(buscarRAMTb);
		
		lblBuscarRam = new JLabel("Buscar:");
		lblBuscarRam.setBounds(10, 28, 191, 14);
		contentPane.add(lblBuscarRam);
		
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}


	public JButton getGuardarBtn() {
		return guardarBtn;
	}

	public JTextField getBuscarRAMTb() {
		return buscarRAMTb;
	}
	
	
	

}
