package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class PersonaView extends JFrame {
		private JTextField curpTF;
		private JTextField nombreTF;
		private JTextField apPatTF;
		private JTextField apMatTF;
		private JButton guardarBtn;

		
		
		public PersonaView() {
			
			
			super("Alta Persona");
			this.setSize(400, 400);
			this.setLayout(null);
			JLabel curpLabel = new JLabel("CURP: ");
			curpLabel.setBounds(50, 50, 100, 50);
			this.getContentPane().add(curpLabel);
			curpTF = new JTextField();
			curpTF.setBounds(150, 65, 200, 20);
			this.getContentPane().add(curpTF);

			JLabel nombreLabel = new JLabel("Nombre: ");
			nombreLabel.setBounds(50, 100, 100, 50);
			this.getContentPane().add(nombreLabel);
			nombreTF = new JTextField();
			nombreTF.setBounds(150, 115, 200, 20);
			this.getContentPane().add(nombreTF);

			JLabel apPatLabel = new JLabel("A. Paterno: ");
			apPatLabel.setBounds(50, 150, 100, 50);
			this.getContentPane().add(apPatLabel);
			apPatTF = new JTextField();
			apPatTF.setBounds(150, 165, 200, 20);
			this.getContentPane().add(apPatTF);

			JLabel apMatLabel = new JLabel("A. Materno: ");
			apMatLabel.setBounds(50, 200, 100, 50);
			this.getContentPane().add(apMatLabel);
			this.setVisible(true);
			apMatTF = new JTextField();
			apMatTF.setBounds(150, 215, 200, 20);
			this.getContentPane().add(apMatTF);

			guardarBtn = new JButton("Guardar");
			guardarBtn.setBounds(270, 270, 100, 20);
			this.getContentPane().add(guardarBtn);
			
			


		}

		public static void main(String args[]) {
			PersonaView view = new PersonaView();
		}

		public JTextField getCurpTF() {
			return curpTF;
		}

		public JTextField getNombreTF() {
			return nombreTF;
		}

		public JTextField getApPatTF() {
			return apPatTF;
		}

		public JTextField getApMatTF() {
			return apMatTF;
		}

		public JButton getGuardarBtn() {
			return guardarBtn;
		}
		public void Limpiar() {
			curpTF.setText("");
			this.apMatTF.setText("");
			this.apPatTF.setText("");
			this.nombreTF.setText("");
			
		}
}
