package controller;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import model.daos.IPersonaDAO;
import model.entities.Persona;
import view.PersonaView;

public class NuevaPersonaController {
	IPersonaDAO dao;
	PersonaView vista;
	
	public NuevaPersonaController(IPersonaDAO dao, PersonaView vista) {
		this.dao=dao;
		this.vista=vista;
		this.vista.getGuardarBtn().addActionListener(e->agregarPersona());
	}
	
	public void agregarPersona() {
	
		int confirmacion = JOptionPane.showConfirmDialog(null, "�Desea Guardar?");
		if(JOptionPane.OK_OPTION == confirmacion) {
			String curp = vista.getCurpTF().getText();
			String nombre = vista.getNombreTF().getText();
			String apPaterno = vista.getApPatTF().getText();
			String apMat = vista.getApMatTF().getText();
			
			Persona p = new Persona(curp, nombre, apPaterno, apMat);
		
			try{
				dao.agregarPersona(p);
				JOptionPane.showMessageDialog(null, "Transacci�n Exitosa");
				this.vista.Limpiar();
			}catch(SQLException e){
				JOptionPane.showMessageDialog(null, "Error al agregar a la Persona"+e);
			}

		}
	}
	

}
